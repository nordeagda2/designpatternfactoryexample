package car;

/**
 * Created by amen on 8/8/17.
 */
public abstract class CarFactory {

    public static Car createBMW16(){
        return new Car("BMW", 30.0, 200, 4);
    }
}
