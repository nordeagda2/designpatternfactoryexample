package car;

/**
 * Created by amen on 8/8/17.
 */
public interface ICar {
    void drive();
    int getSpeed();
    double getGasLevel();
    double getHorsepower();

}
